﻿using System;

namespace lab5.domain
{
    public class Record : EntityBase
    {
        public virtual DateTime Date { get; set; }
        public virtual Doctor Doctor { get; set; }
        public virtual Patient Patient { get; set; }
    }
}