﻿using System.Collections.Generic;

namespace lab5.domain
{
    public class Doctor : EntityBase
    {
        public virtual string DoctorName { get; set; }
        public virtual string Specialty { get; set; }
        public virtual int Year { get; set; }
        public virtual IList<Record> Record { get; set; } = new List<Record>();
    }
}