﻿using System.Collections.Generic;

namespace lab5.domain
{
    public class Patient : EntityBase
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual char Sex { get; set; }
        public virtual int Year { get; set; }
        public virtual IList<Record> Record { get; set; } = new List<Record>();
    }
}