﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using lab5.dao;
using lab5.domain;
using NHibernate;

namespace lab5
{
    public partial class DoctorPage : Page
    {
        private IDoctorDAO DoctorDao
        {
            get
            {
                var session = (ISession) Session["hbmsession"];
                var factory = new NHibernateDAOFactory(session);
                return factory.getDoctorDAO();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Prerender(object sender, EventArgs e)
        {
            GridView1.DataSource = DoctorDao.GetAll();
            GridView1.DataBind();
        }

        private void AddDoctor(string name, string specialty, string year)
        {
            var doctor = new Doctor
            {
                DoctorName = name,
                Specialty = specialty,
                Year = Convert.ToInt32(year)
            };
            DoctorDao.SaveOrUpdate(doctor);
        }

        private void Refresh()
        {
            Response.Redirect(HttpContext.Current.Request.Url.ToString());
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var row = GridView1.Rows[e.RowIndex];
            var key = ((Label) row.Cells[0].FindControl("myLabel1")).Text;
            var doctor = DoctorDao.getDoctorByName(key);
            //Удаление
            if (doctor != null)
            {
                DoctorDao.Delete(doctor);
            }
            Refresh();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            var index = e.NewEditIndex;
            var row = GridView1.Rows[index];
            var oldGroupName = ((Label) row.Cells[0].FindControl("myLabel1")).Text;
            ViewState["oldGroupName"] = oldGroupName;
            GridView1.EditIndex = index;
            GridView1.ShowFooter = false;
            GridView1.DataBind();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var index = e.RowIndex;
            var row = GridView1.Rows[index];
            var newDoctorName = ((TextBox) row.Cells[1].FindControl("myTextBox1")).Text;
            var newSpecName = ((TextBox) row.Cells[2].FindControl("myTextBox2")).Text;
            var newYearName = ((TextBox) row.Cells[3].FindControl("myTextBox3")).Text;
            var oldGroupName = (string) ViewState["oldGroupName"];

            var doctor = DoctorDao.getDoctorByName(oldGroupName);
            doctor.DoctorName = newDoctorName;
            doctor.Specialty = newSpecName;
            doctor.Year = int.Parse(newYearName);
            DoctorDao.SaveOrUpdate(doctor);
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        #region Insert

        protected void ibInsert_Click(object sender, ImageClickEventArgs e)
        {
            var name = ((TextBox) GridView1.FooterRow.FindControl("MyFooterTextBox1")).Text;
            var specialty = ((TextBox) GridView1.FooterRow.FindControl("MyFooterTextBox2")).Text;
            var year = ((TextBox) GridView1.FooterRow.FindControl("MyFooterTextBox3")).Text;
            AddDoctor(name, specialty, year);
            Refresh();
        }

        protected void ibInsertInEmpty_Click(object sender, ImageClickEventArgs e)
        {
            var parent = ((Control) sender).Parent;
            var name = ((TextBox) parent.FindControl("emptyGroupNameTextBox")).Text;
            var specialty = ((TextBox) parent.FindControl("emptyCuratorNameTextBox")).Text;
            var year = ((TextBox) parent.FindControl("emptyHeadmanNameTextBox")).Text;
            AddDoctor(name, specialty, year);
            Refresh();
        }

        #endregion
    }
}