﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using lab5.dao;
using lab5.domain;
using NHibernate;

namespace lab5
{
    public partial class Default : Page
    {
        private IRecordDao RecordDao => GetFactory().GetRecordDao();
        private IDoctorDAO DoctorDao => GetFactory().getDoctorDAO();
        private IPatientDAO PatientDao => GetFactory().getPatientDAO();

        private NHibernateDAOFactory GetFactory()
        {
            var session = (ISession) Session["hbmsession"];
            var factory = new NHibernateDAOFactory(session);
            return factory;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Prerender(object sender, EventArgs e)
        {
            GridView1.DataSource = RecordDao.GetAll();
            GridView1.DataBind();
        }

        protected void ibInsert_Click(object sender, ImageClickEventArgs e)
        {
            var date = ((TextBox) GridView1.FooterRow.FindControl("MyFooterTextBox2")).Text;
            var idPatient = Convert.ToInt32(((TextBox) GridView1.FooterRow.FindControl("MyFooterTextBox3")).Text);
            var idDoctor = Convert.ToInt32(((TextBox) GridView1.FooterRow.FindControl("MyFooterTextBox4")).Text);
            SaveRecord(idPatient, idDoctor, date);
        }

        private void SaveRecord(int idPatient, int idDoctor, string date)
        {
            var patient = PatientDao.GetById(idPatient);
            var doctor = DoctorDao.GetById(idDoctor);
            if (patient == null || doctor == null) return;

            var record = new Record
            {
                Date = DateTime.Parse(date)
            };
            patient.Record.Add(record);
            doctor.Record.Add(record);
            record.Doctor = doctor;
            record.Patient = patient;
            RecordDao.SaveOrUpdate(record);
            Refresh();
        }

        protected void ibInsertInEmpty_Click(object sender, ImageClickEventArgs e)
        {
            var parent = ((Control) sender).Parent;
            var date = (parent.FindControl("emptyDate") as TextBox)?.Text;
            var idPatient = Convert.ToInt32((parent.FindControl("emptyIdPatient") as TextBox)?.Text);
            var idDoctor = Convert.ToInt32((parent.FindControl("emptyIdDoctor") as TextBox)?.Text);
            SaveRecord(idPatient, idDoctor, date);
        }

        private void Refresh()
        {
            Response.Redirect(HttpContext.Current.Request.Url.ToString());
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var row = GridView1.Rows[e.RowIndex];
            var id = Convert.ToInt32(((Label) row.Cells[0].FindControl("myLabel1"))?.Text);
            var record = RecordDao.GetById(id);
            if (record != null)
            {
                RecordDao.Delete(record);
            }
            Refresh();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Получить индекс выделенной строки
            var index = e.NewEditIndex;
            var row = GridView1.Rows[index];
            //Получение старых значений полей в строке GridView
            ViewState["oldGroupName"] = ((Label) row.Cells[0].FindControl("myLabel1")).Text;
            GridView1.EditIndex = index;
            GridView1.ShowFooter = false;
            GridView1.DataBind();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var index = e.RowIndex;
            var row = GridView1.Rows[index];
            var date = ((TextBox) row.Cells[1].FindControl("myTextBox2")).Text;
            var id = Convert.ToInt32(((Label) row.Cells[0].FindControl("myTextBox1")).Text);
            var idPatient = Convert.ToInt32(((TextBox) row.Cells[2].FindControl("myTextBox3")).Text);
            var idDoctor = Convert.ToInt32(((TextBox) row.Cells[3].FindControl("myTextBox4")).Text);
            var record = RecordDao.GetById(id);
            var doctor = DoctorDao.GetById(idDoctor);
            var patient = PatientDao.GetById(idPatient);
            if (record != null && doctor != null && patient != null)
            {
                record.Patient = patient;
                record.Doctor = doctor;
                record.Date = DateTime.Parse(date);

                patient.Record.Add(record);
                doctor.Record.Add(record);
                RecordDao.SaveOrUpdate(record);
            }
            Reset();
        }

        private void Reset()
        {
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }
    }
}