﻿using System.Collections.Generic;
using lab5.domain;
using NHibernate;
using NHibernate.Criterion;

namespace lab5.dao
{
    public class DoctorDAO : GenericDAO<Doctor>, IDoctorDAO
    {
        public DoctorDAO(ISession session) : base(session)
        {
        }


        public Doctor getDoctorByName(string doctorName)
        {
            var doctor = new Doctor();
            doctor.DoctorName = doctorName;
            var criteria = session.CreateCriteria(typeof(Doctor))
                .Add(Restrictions.Eq("DoctorName", doctorName));
            var list = criteria.List<Doctor>();
            doctor = list[0];
            return doctor;
        }

        public IList<Patient> getAllPatientsOfDoctor(string doctorName)
        {
            var list = session.CreateSQLQuery(
                "SELECT Patients.* FROM Patients JOIN Doctors" +
                " ON Patients.DoctorId = Doctors.Id" +
                " WHERE Doctors.DoctorName='" + doctorName + "'")
                .AddEntity("Patient", typeof(Patient))
                .List<Patient>();
            return list;
        }

        public void delDoctorByName(string doctorName)
        {
            var doctor = getDoctorByName(doctorName);
            Delete(doctor);
        }
    }
}