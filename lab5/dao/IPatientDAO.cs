﻿using System.Collections.Generic;
using lab5.domain;

namespace lab5.dao
{
    public interface IPatientDAO : IGenericDAO<Patient>
    {
        IList<Patient> searchPatientsByFirstNameOrLastName(string search);
        IList<Patient> searchPatientsByFirstNameAndLastName(string firstname, string lastname);
    }
}