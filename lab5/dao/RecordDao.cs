using lab5.domain;
using NHibernate;

namespace lab5.dao
{
    public class RecordDao : GenericDAO<Record>, IRecordDao
    {
        public RecordDao(ISession session1) : base(session1)
        {
        }
    }
}