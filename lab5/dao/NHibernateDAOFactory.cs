﻿using NHibernate;

namespace lab5.dao
{
    public class NHibernateDAOFactory : DAOFactory
    {
        /** NHibernate sessionFactory */
        protected ISession session;

        public NHibernateDAOFactory(ISession session)
        {
            this.session = session;
        }

        public override IDoctorDAO getDoctorDAO()
        {
            return new DoctorDAO(session);
        }

        public override IPatientDAO getPatientDAO()
        {
            return new PatientDAO(session);
        }

        public override IRecordDao GetRecordDao()
        {
            return new RecordDao(session);
        }
    }
}