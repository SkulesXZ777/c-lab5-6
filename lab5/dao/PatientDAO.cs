﻿using System.Collections.Generic;
using lab5.domain;
using NHibernate;

namespace lab5.dao
{
    public class PatientDAO : GenericDAO<Patient>, IPatientDAO
    {
        public PatientDAO(ISession session) : base(session)
        {
        }

        public IList<Patient> searchPatientsByFirstNameOrLastName(string search)
        {
            return session.CreateSQLQuery(
                "SELECT * FROM patients" +
                " WHERE CONCAT(firstname,' ', lastname) LIKE '%" + search + "%'")
                .AddEntity("Patient", typeof(Patient))
                .List<Patient>();
        }

        public IList<Patient> searchPatientsByFirstNameAndLastName(string firstname, string lastname)
        {
            return session.CreateSQLQuery(
                "SELECT * FROM patients" +
                " Where \"firstname\" LIKE '%" + firstname + "%' AND \"lastname\" LIKE '%" + lastname + "%'")
                .AddEntity("Patient", typeof(Patient))
                .List<Patient>();
        }
    }
}