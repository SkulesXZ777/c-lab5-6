﻿using System.Collections.Generic;
using lab5.domain;

namespace lab5.dao
{
    public interface IDoctorDAO : IGenericDAO<Doctor>
    {
        Doctor getDoctorByName(string doctorName);
        IList<Patient> getAllPatientsOfDoctor(string doctorName);
        void delDoctorByName(string doctorName);
    }
}