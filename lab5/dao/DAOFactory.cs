﻿namespace lab5.dao
{
    public abstract class DAOFactory
    {
        public abstract IDoctorDAO getDoctorDAO();
        public abstract IPatientDAO getPatientDAO();
        public abstract IRecordDao GetRecordDao();
    }
}