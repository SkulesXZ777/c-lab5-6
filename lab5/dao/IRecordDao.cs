using lab5.domain;

namespace lab5.dao
{
    public interface IRecordDao : IGenericDAO<Record>
    {
    }
}