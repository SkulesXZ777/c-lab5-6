﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using lab5.dao;
using lab5.domain;
using NHibernate;

namespace lab5
{
    public partial class PatientForm : Page
    {
        private IPatientDAO PatientDao
        {
            get
            {
                var session = (ISession) Session["hbmsession"];
                DAOFactory factory = new NHibernateDAOFactory(session);
                return factory.getPatientDAO();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Prerender(object sender, EventArgs e)
        {
            GridView1.DataSource = PatientDao.GetAll();
            GridView1.DataBind();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var index = e.RowIndex;
            var row = GridView1.Rows[index];
            var id = Convert.ToInt32(((Label) row.Cells[0].FindControl("labelId")).Text);
            var patient = PatientDao.GetById(id);
            if (patient != null)
            {
                PatientDao.Delete(patient);
            }
            Refresh();
        }

        private void Refresh()
        {
            Response.Redirect(HttpContext.Current.Request.Url.ToString());
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Получить индекс выделенной строки
            var index = e.NewEditIndex;
            var row = GridView1.Rows[index];
            //Получение старых значений полей в строке GridView
            var oldFirstName = ((Label) row.Cells[0].FindControl("myLabel1")).Text;
            var oldLastName = ((Label) row.Cells[1].FindControl("myLabel2")).Text;
            //Сохранение названия в коллекции ViewState
            ViewState["oldFirstName"] = oldFirstName;
            ViewState["oldLastName"] = oldLastName;
            GridView1.EditIndex = index;
            GridView1.ShowFooter = false;
            GridView1.DataBind();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var index = e.RowIndex;
            var row = GridView1.Rows[index];
            var newFirstName = ((TextBox) row.Cells[1].FindControl("myTextBox1")).Text;
            var newLastName = ((TextBox) row.Cells[2].FindControl("myTextBox2")).Text;
            var newSex = ((TextBox) row.Cells[3].FindControl("myTextBox3")).Text;
            var newYear = ((TextBox) row.Cells[4].FindControl("myTextBox4")).Text;
            var id = Convert.ToInt32(((Label) row.Cells[0].FindControl("labelEditId")).Text);

            var patient = PatientDao.GetById(id);
            if (patient != null)
            {
                patient.FirstName = newFirstName;
                patient.LastName = newLastName;
                patient.Sex = newSex[0];
                patient.Year = Convert.ToInt32(newYear);
                PatientDao.SaveOrUpdate(patient);
            }
            IndexChanging();
        }

        private void IndexChanging()
        {
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            IndexChanging();
        }

        protected void ibInsert_Click(object sender, ImageClickEventArgs e)
        {
            var control = GridView1.FooterRow;
            var patient = new Patient
            {
                Sex = GetControlText(control, "MyFooterTextBox3").ToCharArray(0, 1)[0],
                Year = Convert.ToInt32(GetControlText(control, "MyFooterTextBox4")),
                FirstName = GetControlText(control, "MyFooterTextBox1"),
                LastName = GetControlText(control, "MyFooterTextBox2")
            };
            PatientDao.SaveOrUpdate(patient);
            Response.Redirect(HttpContext.Current.Request.Url.ToString());
        }

        protected void ibInsertInEmpty_Click(object sender, ImageClickEventArgs e)
        {
            var control = ((Control) sender).Parent;
            var patient = new Patient
            {
                FirstName = GetControlText(control, "emptyFirstNameTextBox"),
                LastName = GetControlText(control, "emptyLastNameTextBox"),
                Sex = GetControlText(control, "emptySexTextBox").ToCharArray(0, 1)[0],
                Year = Convert.ToInt32(GetControlText(control, "emptyYearTextBox"))
            };
            PatientDao.SaveOrUpdate(patient);
            Response.Redirect(HttpContext.Current.Request.Url.ToString());
        }

        private static string GetControlText(Control parent, string controlName)
        {
            var findControl = (TextBox) parent?.FindControl(controlName);
            return findControl?.Text;
        }
    }
}