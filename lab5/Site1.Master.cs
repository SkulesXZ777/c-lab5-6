﻿using System;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace lab5
{
    public partial class Site1 : MasterPage
    {
        private ISessionFactory factory;
        private ISession session;

//Метод открытия сессии
        private ISession openSession()
        {
            Assembly mappingsAssemly = Assembly.GetExecutingAssembly();
                        if (factory == null)
                            {
                                //Конфигурирование фабрики сессий
                factory = Fluently.Configure()
                                .Database(PostgreSQLConfiguration
                                .PostgreSQL82.ConnectionString(c => c
                                .Host("127.0.0.1")
                                .Port(5432)
                                .Database("laba5guard")
                                .Username("postgres")
                                .Password("postgres")))
                                .Mappings(m => m.FluentMappings
                                .AddFromAssembly(mappingsAssemly))
                                //.ExposeConfiguration(BuildSchema)
                                .BuildSessionFactory();
                            }
                        //Открытие сессии
            session = factory.OpenSession();
            return session;
        }

        //Метод для автоматического создания таблиц в базе данных
        private static void BuildSchema(Configuration config)
        {
            new SchemaExport(config).Create(false, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            session = openSession();
            Session["hbmsession"] = session;
        }

        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            Response.Redirect("/");
        }

        protected void searchImageButton_OnClick(object sender, ImageClickEventArgs e)
        {
        }

        protected void TextBox3_OnTextChanged(object sender, EventArgs e)
        {
        }
    }
}