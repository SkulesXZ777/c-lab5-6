﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="lab5.Default" MasterPageFile="~/Site1.Master"%>

<asp:Content ID="Content1"
             ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center">
        <!-- Строка названия таблицы-->
        <tr align="center">
            <td>
                <asp:Label ID="Label4" runat="server" Text="Записи"
                           Font-Size="20pt" ForeColor="Maroon" Font-Bold="True"/>
            </td>
        </tr>
        <!--Строка, содержащая таблицу для отображения списка групп-->
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server"
                              AutoGenerateColumns="false"
                              ShowFooter="true" ShowHeader="true"
                              AllowPaging="true" PageSize="10"
                              onrowdeleting="GridView1_RowDeleting" Font-Size="14pt"
                              onrowediting="GridView1_RowEditing"
                              onrowcancelingedit="GridView1_RowCancelingEdit"
                              onpageindexchanging="GridView1_PageIndexChanging"
                              HorizontalAlign="Center"
                              onrowupdating="GridView1_RowUpdating"
                              ItemType="lab5.domain.Record">
                    <Columns>

                        <asp:TemplateField HeaderText="Id"
                                           ItemStyle-Width="75">
                            <ItemTemplate>
                                <asp:Label id="myLabel1" runat="server" Text='<%# Bind("Id") %>'/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="myTextBox1" runat="server" Width="75" Text='<%# Bind("Id") %>'/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="myFooterTextBox1" runat="server" Width="75" Text='<%# Bind("Id") %>'/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Время"
                                           ItemStyle-Width="300">
                            <ItemTemplate>
                                <asp:Label id="myLabel2" runat="server" Text='<%# Bind("Date") %>'/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="myTextBox2" runat="server" Width="300" Text='<%# Bind("Date") %>'/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="myFooterTextBox2" runat="server" Width="300" Text='<%# Bind("Date") %>'/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Ид пациента"
                                           ItemStyle-Width="75">
                            <ItemTemplate>
                                <asp:Label id="myLabel3" runat="server" Text="<%# BindItem.Patient.Id %>"/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="myTextBox3" runat="server" Width="75" Text="<%# BindItem.Patient.Id %>"/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="myFooterTextBox3" runat="server" Width="75" Text="<%# BindItem.Patient.Id %>"/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Фамилия пациента"
                                           ItemStyle-Width="300">
                            <ItemTemplate>
                                <asp:Label id="myLabel31" runat="server" Text="<%# BindItem.Patient.LastName %>"/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="myTextBox31" runat="server" Width="300" Text="<%# BindItem.Patient.LastName %>"/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="myFooterTextBox31" runat="server" Width="300" Text="<%# BindItem.Patient.LastName %>"/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Ид доктора"
                                           ItemStyle-Width="75">
                            <ItemTemplate>
                                <asp:Label id="myLabel4" runat="server" Text="<%# BindItem.Doctor.Id %>"/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="myTextBox4" runat="server" Width="75" Text="<%# BindItem.Doctor.Id %>"/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="myFooterTextBox4" runat="server" Width="75" Text="<%# BindItem.Doctor.Id %>"/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ФИО доктора"
                                           ItemStyle-Width="300">
                            <ItemTemplate>
                                <asp:Label id="myLabel41" runat="server" Text="<%# BindItem.Doctor.DoctorName %>"/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="myTextBox41" runat="server" Width="300" Text="<%# BindItem.Doctor.DoctorName %>"/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="myFooterTextBox41" runat="server" Width="300" Text="<%# BindItem.Doctor.DoctorName %>"/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Команды" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibEdit" runat="server"
                                                 CommandName="Edit" Text="Edit"
                                                 ImageUrl="~/img/6.png"/>
                                <asp:ImageButton ID="ibDelete" runat="server"
                                                 CommandName="Delete" Text="Delete"
                                                 ImageUrl="~/img/3.png"/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibUpdate" runat="server"
                                                 CommandName="Update" Text="Update"
                                                 ImageUrl="~/img/5.png"/>
                                <asp:ImageButton ID="ibCancel" runat="server"
                                                 CommandName="Cancel" Text="Cancel"
                                                 ImageUrl="~/img/7.png"/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibInsert" runat="server"
                                                 CommandName="Insert" OnClick="ibInsert_Click"
                                                 ImageUrl="~/img/2.png"/>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <EmptyDataTemplate>
                        <table border="1" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="200" align="center">Id</td>
                                <td width="300" align="center">Время</td>
                                <td width="200" align="center">Ид пациента</td>
                                <td width="200" align="center">Ид доктора</td>
                                <td>Команды</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="emptyId" runat="server"
                                               Width="200"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="emptyDate" runat="server"
                                                 Width="300"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="emptyIdPatient" runat="server"
                                                 Width="200"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="emptyIdDoctor" runat="server"
                                                 Width="200"/>
                                </td>
                                <td align="center">
                                    <asp:ImageButton ID="emptyImageButton" runat="server"
                                                     ImageUrl="~/img/2.png"
                                                     OnClick="ibInsertInEmpty_Click"/>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <PagerStyle HorizontalAlign="Center"/>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <script>
        $(function() {
            $('#ContentPlaceHolder1_GridView1_myFooterTextBox2')
                .datepicker(
                {
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2100'
                });
            $('#ContentPlaceHolder1_GridView1_myTextBox2')
                .datepicker(
                {
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2100'
                });
            $('#ContentPlaceHolder1_GridView1_emptyDate')
                .datepicker(
                {
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2100'
                });
        })
    </script>

</asp:Content>