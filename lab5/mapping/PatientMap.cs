﻿using FluentNHibernate.Mapping;
using lab5.domain;

namespace lab5.mapping
{
    public class PatientMap : ClassMap<Patient>
    {
        public PatientMap()
        {
            Table("Patients");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.Sex);
            Map(x => x.Year);
            HasMany(x => x.Record)
                .Cascade.All()
                .KeyColumn("patient_id");
        }
    }
}