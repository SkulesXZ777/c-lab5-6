﻿using FluentNHibernate.Mapping;
using lab5.domain;

namespace lab5.mapping
{
    public class DoctorMap : ClassMap<Doctor>
    {
        public DoctorMap()
        {
            Table("Doctors");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.DoctorName);
            Map(x => x.Specialty);
            Map(x => x.Year);
            HasMany(x => x.Record)
                .Cascade.All()
                .KeyColumn("doctor_id");
        }
    }
}