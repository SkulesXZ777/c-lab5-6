﻿using FluentNHibernate.Mapping;
using lab5.domain;

namespace lab5.mapping
{
    public class RecordMap : ClassMap<Record>
    {
        public RecordMap()
        {
            Table("Record");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Date);
            References(x => x.Patient, "patient_id");
            References(x => x.Doctor, "doctor_id");
        }
    }
}