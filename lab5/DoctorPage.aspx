﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoctorPage.aspx.cs" Inherits="lab5.DoctorPage" MasterPageFile="~/Site1.Master"%>

<asp:Content ID="Content1"
             ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center">
        <!-- Строка названия таблицы-->
        <tr align="center">
            <td>
                <asp:Label ID="Label4" runat="server" Text="Список врачей"
                           Font-Size="20pt" ForeColor="Maroon" Font-Bold="True"/>
            </td>
        </tr>
        <!--Строка, содержащая таблицу для отображения списка групп-->
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server"
                              AutoGenerateColumns="false"
                              ShowFooter="true" ShowHeader="true"
                              AllowPaging="true" PageSize="10"
                              onrowdeleting="GridView1_RowDeleting" Font-Size="14pt"
                              onrowediting="GridView1_RowEditing"
                              onrowcancelingedit="GridView1_RowCancelingEdit"
                              onpageindexchanging="GridView1_PageIndexChanging"
                              HorizontalAlign="Center"
                              onrowupdating="GridView1_RowUpdating">
                    <Columns>
                        <asp:TemplateField HeaderText="Id"
                                           ItemStyle-Width="75">
                            <ItemTemplate>
                                <asp:Label id="labelId" runat="server" Text='<%# Bind("Id") %>'/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="textBoxId" runat="server" Width="200" Text='<%# Bind("Id") %>'/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="footerTextBoxId" runat="server" Width="200" Text='<%# Bind("Id") %>'/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ФИО Доктора"
                                           ItemStyle-Width="200">
                            <ItemTemplate>
                                <asp:Label id="myLabel1" runat="server" Text='<%# Bind("DoctorName") %>'/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="myTextBox1" runat="server" Width="200" Text='<%# Bind("DoctorName") %>'/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="myFooterTextBox1" runat="server" Width="200" Text='<%# Bind("DoctorName") %>'/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Специальность"
                                           ItemStyle-Width="300">
                            <ItemTemplate>
                                <asp:Label id="myLabel2" runat="server" Text='<%# Bind("Specialty") %>'/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="myTextBox2" runat="server" Width="300" Text='<%# Bind("Specialty") %>'/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="myFooterTextBox2" runat="server" Width="300" Text='<%# Bind("Specialty") %>'/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Год рождения"
                                           ItemStyle-Width="300">
                            <ItemTemplate>
                                <asp:Label id="myLabel3" runat="server" Text='<%# Bind("Year") %>'/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="myTextBox3" runat="server" Width="300" Text='<%# Bind("Year") %>'/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="myFooterTextBox3" runat="server" Width="300" Text='<%# Bind("Year") %>'/>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Команды" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibEdit" runat="server"
                                                 CommandName="Edit" Text="Edit"
                                                 ImageUrl="~/img/6.png"/>
                                <asp:ImageButton ID="ibDelete" runat="server"
                                                 CommandName="Delete" Text="Delete"
                                                 ImageUrl="~/img/3.png"/>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibUpdate" runat="server"
                                                 CommandName="Update" Text="Update"
                                                 ImageUrl="~/img/5.png"/>
                                <asp:ImageButton ID="ibCancel" runat="server"
                                                 CommandName="Cancel" Text="Cancel"
                                                 ImageUrl="~/img/7.png"/>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibInsert" runat="server"
                                                 CommandName="Insert" OnClick="ibInsert_Click"
                                                 ImageUrl="~/img/2.png"/>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <EmptyDataTemplate>
                        <table border="1" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="75" align="center">Id</td>
                                <td width="200" align="center">Фамилия</td>
                                <td width="300" align="center">Специальность</td>
                                <td width="300" align="center">Год</td>
                                <td>Список докторов</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="emtyId" runat="server"
                                               Width="75"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="emptyGroupNameTextBox" runat="server"
                                                 Width="200"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="emptyCuratorNameTextBox" runat="server"
                                                 Width="300"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="emptyHeadmanNameTextBox" runat="server"
                                                 Width="300"/>
                                </td>
                                <td align="center">
                                    <asp:ImageButton ID="emptyImageButton" runat="server"
                                                     ImageUrl="~/img/2.png"
                                                     OnClick="ibInsertInEmpty_Click"/>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <PagerStyle HorizontalAlign="Center"/>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>