﻿using System;
using lab5.dao;
using lab5.domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DaoTestProject
{
    [TestClass]
    public class TestRecordDAO : TestGenericDAO<Record>
    {
        protected Doctor doctor;
        protected IDoctorDAO doctorDAO;
        protected IPatientDAO patientDAO;
        protected IRecordDao recordDAO;

        public TestRecordDAO()
        {
            DAOFactory daoFactory = new NHibernateDAOFactory(session);
            patientDAO = daoFactory.getPatientDAO();
            doctorDAO = daoFactory.getDoctorDAO();
            recordDAO = daoFactory.GetRecordDao();
            setDAO(recordDAO);
        }

        protected override void createEntities()
        {
            Doctor d = doctorDAO.GetAll()[0];
            Patient p = patientDAO.GetAll()[0];

            entity1 = new Record();
            entity1.Date = new DateTime();
            entity1.Patient = p;
            entity1.Doctor = d;

            entity2 = new Record();
            entity2.Date = new DateTime();
            entity2.Patient = p;
            entity2.Doctor = d;

            entity3 = new Record();
            entity3.Date = new DateTime();
            entity3.Patient = p;
            entity3.Doctor = d;
        }

        protected override void checkAllPropertiesDiffer(Record entityToCheck1, Record entityToCheck2)
        {
            Assert.AreEqual(entityToCheck1.Id,entityToCheck1.Id, "Values must be different");

        }

        protected override void checkAllPropertiesEqual(Record entityToCheck1, Record entityToCheck2)
        {
            Assert.AreNotEqual(entityToCheck1.Id, entityToCheck2.Date, "Values must be equal");
        }
     
        [TestMethod]
        public void TestGetByIdRecord()
        {
            TestGetByIdGeneric();
        }

        [TestMethod]
        public void TestGetAllRecord()
        {
            TestGetAllGeneric();
        }

        [TestMethod]
        public void TestDeleteRecord()
        {
            TestDeleteGeneric();
        }
    }
}