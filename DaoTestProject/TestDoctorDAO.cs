﻿using System;
using lab5.dao;
using lab5.domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DaoTestProject
{
    [TestClass]
    public class TestDoctorDAO : TestGenericDAO<Doctor>
    {
        protected IDoctorDAO doctorDAO;
        protected Patient patient1;
        protected Patient patient2;
        protected Patient patient3;

        public TestDoctorDAO()
        {
            DAOFactory daoFactory = new NHibernateDAOFactory(session);
            doctorDAO = daoFactory.getDoctorDAO();
            setDAO(doctorDAO);
        }

        protected override void createEntities()
        {
            entity1 = new Doctor();
            entity1.DoctorName = "Скулес А.Х.";
            entity1.Specialty = "Главврач";
            entity1.Year = 1966;

            entity2 = new Doctor();
            entity2.DoctorName = "Попов А.В.";
            entity2.Specialty = "Окулист";
            entity2.Year = 1976;

            entity3 = new Doctor();
            entity3.DoctorName = "Иванов В.В.";
            entity3.Specialty = "Лор";
            entity3.Year = 1955;
        }

        protected override void checkAllPropertiesDiffer(Doctor entityToCheck1, Doctor entityToCheck2)
        {
            Assert.AreNotEqual(entityToCheck1.DoctorName, entityToCheck2.DoctorName, "Values must be different");
            Assert.AreNotEqual(entityToCheck1.Specialty, entityToCheck2.Specialty, "Values must be different");
            Assert.AreNotEqual(entityToCheck1.Year, entityToCheck2.Year, "Values must be different");
        }

        protected override void checkAllPropertiesEqual(Doctor entityToCheck1, Doctor entityToCheck2)
        {
            Assert.AreEqual(entityToCheck1.DoctorName, entityToCheck2.DoctorName, "Values must be equal");
            Assert.AreEqual(entityToCheck1.Specialty, entityToCheck2.Specialty, "Values must be equal");
            Assert.AreEqual(entityToCheck1.Year, entityToCheck2.Year, "Values must be equal");
        }

        [TestMethod]
        public void TestGetByIdDoctor()
        {
            TestGetByIdGeneric();
        }

        [TestMethod]
        public void TestGetAllDoctors()
        {
            TestGetAllGeneric();
        }

        [TestMethod]
        public void TestDeleteDoctor()
        {
            TestDeleteGeneric();
        }

        [TestMethod]
        public void TestGetDoctorByName()
        {
            var doctor1 = doctorDAO.getDoctorByName(entity1.DoctorName);
            Assert.IsNotNull(doctor1);
            var doctor2 = doctorDAO.getDoctorByName(entity2.DoctorName);
            Assert.IsNotNull(doctor2);
            var doctor3 = doctorDAO.getDoctorByName(entity3.DoctorName);
            Assert.IsNotNull(doctor3);
            checkAllPropertiesEqual(doctor1, entity1);
            checkAllPropertiesEqual(doctor2, entity2);
            checkAllPropertiesEqual(doctor3, entity3);
        }
        
        [TestMethod]
        public void TestDelDoctorByName()
        {
            try
            {
                doctorDAO.delDoctorByName(entity2.DoctorName);
            }
            catch (Exception)
            {
                Assert.Fail("Deletion should be successful of entity2");
            }
            // Checking if other two entities can be still found
            getEntityGeneric(entity1.Id, entity1);
            getEntityGeneric(entity3.Id, entity3);
            // Checking if entity2 can not be found
            try
            {
                Doctor foundDoctor = null;
                foundDoctor = dao.GetById(entity2.Id);
                Assert.IsNull(foundDoctor,
                    "After deletion entity should not be found with DoctorName " +
                    entity2.DoctorName);
            }
            catch (Exception)
            {
                Assert.Fail("Should return null if finding the deleted entity");
            }
            // Checking if other two entities can still be found in getAll list
            var list = getListOfAllEntities();
            Assert.IsTrue(list.Contains(entity1),
                "After dao method GetAll list should contain entity1");
            Assert.IsTrue(list.Contains(entity3),
                "After dao method GetAll list should contain entity3");
        }

        protected void createEntitiesForPatient()
        {
            patient1 = new Patient();
            patient1.FirstName = "Алексей";
            patient1.LastName = "Петров";
            patient1.Sex = 'М';
            patient1.Year = 1995;

            patient2 = new Patient();
            patient2.FirstName = "Антон";
            patient2.LastName = "Якумов";
            patient2.Sex = 'М';
            patient2.Year = 1995;

            patient3 = new Patient();
            patient3.FirstName = "Валентин";
            patient3.LastName = "Скулесов";
            patient3.Sex = 'М';
            patient3.Year = 1995;
        }

        protected void checkAllPropertiesEqualForPatient(Patient entityToCheck1, Patient entityToCheck2)
        {
            Assert.AreEqual(entityToCheck1.FirstName, entityToCheck2.FirstName,
                "Values must be equal");
            Assert.AreEqual(entityToCheck1.LastName, entityToCheck2.LastName,
                "Values must be equal");
            Assert.AreEqual(entityToCheck1.Sex, entityToCheck2.Sex,
                "Values must be equal");
            Assert.AreEqual(entityToCheck1.Year, entityToCheck2.Year,
                "Values must be equal");
        }
    }
}