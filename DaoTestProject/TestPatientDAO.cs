﻿using System;
using lab5.dao;
using lab5.domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate.Criterion;

namespace DaoTestProject
{
    [TestClass]
    public class TestPatientDAO : TestGenericDAO<Patient>
    {
        protected Doctor doctor;
        protected IDoctorDAO doctorDAO;
        protected IPatientDAO patientDAO;

        public TestPatientDAO()
        {
            DAOFactory daoFactory = new NHibernateDAOFactory(session);
            patientDAO = daoFactory.getPatientDAO();
            doctorDAO = daoFactory.getDoctorDAO();
            setDAO(patientDAO);
        }

        protected override void createEntities()
        {
            entity1 = new Patient();
            entity1.FirstName = "Ivanov";
            entity1.LastName = "Kiril";
            entity1.Sex = 'M';
            entity1.Year = 1995;
            entity2 = new Patient();
            entity2.FirstName = "Petrenko";
            entity2.LastName = "Ivan";
            entity2.Sex = 'M';
            entity2.Year = 1997;
            entity3 = new Patient();
            entity3.FirstName = "Karpov";
            entity3.LastName = "Danil";
            entity3.Sex = 'M';
            entity3.Year = 2000;
        }

        protected override void checkAllPropertiesDiffer(Patient entityToCheck1, Patient entityToCheck2)
        {
            Assert.AreNotEqual(entityToCheck1.FirstName, entityToCheck2.FirstName, "Values must be different");
            Assert.AreNotEqual(entityToCheck1.LastName, entityToCheck2.LastName, "Values must be different");
            Assert.AreNotEqual(entityToCheck1.Year, entityToCheck2.Year, "Values must be different");
        }

        protected override void checkAllPropertiesEqual(Patient entityToCheck1, Patient entityToCheck2)
        {
            Assert.AreEqual(entityToCheck1.FirstName, entityToCheck2.FirstName, "Values must be equal");
            Assert.AreEqual(entityToCheck1.LastName, entityToCheck2.LastName, "Values must be equal");
            Assert.AreEqual(entityToCheck1.Sex, entityToCheck2.Sex, "Values must be equal");
            Assert.AreEqual(entityToCheck1.Year, entityToCheck2.Year, "Values must be equal");
        }

        [TestMethod]
        public void TestGetByIdPatient()
        {
            TestGetByIdGeneric();
        }

        [TestMethod]
        public void TestGetAllPatient()
        {
            TestGetAllGeneric();
        }

        [TestMethod]
        public void TestDeletePatient()
        {
            TestDeleteGeneric();
        }

        protected void getPatientByDoctorFirstNameAndLastName(Patient patient, string DoctorName, string firstName,
            string lastName)
        {
            Patient foundPatient = null;
            try
            {
                //foundPatient = patientDAO.GetPatientByDoctorFirstNameAndLastName(DoctorName, firstName, lastName);
                Assert.IsNotNull(patientDAO, "Service method should return patient if successfull");
                checkAllPropertiesEqual(foundPatient, patient);
            }
            catch (Exception)
            {
                Assert.Fail("Failed to get patient with DoctorName " +
                            DoctorName + " firstName " + firstName + " and lastName " + lastName);
            }
        }

        protected Doctor getPersistentDoctor(Doctor nonPersistentDoctor)
        {
            var criteria = session.CreateCriteria(typeof(Doctor))
                .Add(Example.Create(nonPersistentDoctor));
            var list = criteria.List<Doctor>();
            Assert.IsTrue(list.Count >= 1,
                "Count of grups must be equal or more than 1");
            return list[0];
        }

        protected void checkAllPropertiesEqualDoctor(Doctor entityToCheck1, Doctor entityToCheck2)
        {
            Assert.AreEqual(entityToCheck1.DoctorName, entityToCheck2.DoctorName, "Values must be equal");
            Assert.AreEqual(entityToCheck1.Specialty, entityToCheck2.Specialty, "Values must be equal");
            Assert.AreEqual(entityToCheck1.Year, entityToCheck2.Year, "Values must be equal");
        }
    }
}